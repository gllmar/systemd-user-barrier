SERVICE_NAME=barrierc
echo "$0" "$SERVICE_NAME"
echo "copying service to user service folder"

cp $SERVICE_NAME.service ~/.config/systemd/user/$SERVICE_NAME.service


echo "reloading systemd daemon"
systemctl --user daemon-reload
